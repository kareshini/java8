package dateApi;

import java.time.Clock;
import java.time.Instant;
import java.util.Date;

/**
 * Created by doka on 2017-06-18.
 */
public class ClockAndMillis {
    public static void main(String[] args) {
        Clock clock = Clock.systemDefaultZone();
        long millis = clock.millis(); //millis from 1970-01-01. As System.currentTimeMillis

        //Instant is a proxy to old date api
        Instant instant = clock.instant();
        //instant = Instant.now();

        Date oldDate = Date.from(instant);
    }
}
