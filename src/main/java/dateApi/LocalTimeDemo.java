package dateApi;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

/**
 * Created by doka on 2017-06-18.
 */
public class LocalTimeDemo {
    /*
        LocalTime represents a time without a timezone.
     */
    private static LocalTime nowByTimezone(String timezone){
        ZoneId zone = ZoneId.of(timezone);
        return LocalTime.now(zone);
    }
    public static void main(String[] args) {

        LocalTime europeNow = nowByTimezone("Europe/Berlin");
        LocalTime brazilNow = nowByTimezone("Brazil/East");

        System.out.println("Is europeNow before BrazilNow ? " + europeNow.isBefore(brazilNow));  // false

        long hoursBetween = ChronoUnit.HOURS.between(europeNow, brazilNow);
        long minutesBetween = ChronoUnit.MINUTES.between(europeNow, brazilNow);

        System.out.println("Hours between : " + hoursBetween);
        System.out.println("Minutes between: " + minutesBetween);

        // -------------- Creating specific time -------------------

        LocalTime late = LocalTime.of(23, 59, 59);
        System.out.println(late);       // 23:59:59

        // -------------- Parsing localDate from String ----------------
        DateTimeFormatter germanFormatter =
                DateTimeFormatter
                        .ofLocalizedTime(FormatStyle.SHORT)
                        .withLocale(Locale.GERMAN);

        LocalTime germanTime = LocalTime.parse("13:37", germanFormatter);
        System.out.println(germanTime);   // 13:37

        DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime customTime = LocalTime.parse("01:15 PM", customFormatter);
        System.out.println(customTime); //13:15

        // -------------- Changing time display ----------------
        System.out.println(customTime.format(customFormatter));

        //------------------ Getting details --------------------------

        LocalTime time = LocalTime.of(15,25,10);
        System.out.println("H : " + time.getHour());
        System.out.println("M : " + time.getMinute());
        System.out.println("S : " + time.getSecond());

        //------------------- Time manipulation -----------------------

        System.out.println(time.plusHours(2));//15:25 + 2:00 = 17:25
        System.out.println(time.isBefore(customTime)); // 15:25 is before 13:15 ? false
        System.out.println(time.plus(10,ChronoUnit.MINUTES)); //17:35
    }
}
