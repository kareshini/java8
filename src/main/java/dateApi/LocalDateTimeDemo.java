package dateApi;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Locale;

/**
 * Created by doka on 2017-06-18.
 */
/*
Very similar to LocalDateDemo and to LocalTimeDemo, that why i will not test most of functionalities.
LocalDateTime is combination of date and time.
 */
public class LocalDateTimeDemo {
    public static void main(String[] args) {
        LocalDateTime sylvester = LocalDateTime.of(2014, Month.DECEMBER, 31, 23, 59, 59);

        DayOfWeek dayOfWeek = sylvester.getDayOfWeek();
        System.out.println(dayOfWeek);      // WEDNESDAY

        Month month = sylvester.getMonth();
        System.out.println(month);          // DECEMBER

        long hourOfDay = sylvester.getLong(ChronoField.HOUR_OF_DAY);
        System.out.println(hourOfDay);    // 23
        long minuteOfDay = sylvester.getLong(ChronoField.MINUTE_OF_DAY);
        System.out.println(minuteOfDay);    // 1439

        DateTimeFormatter formatter =
                DateTimeFormatter
                        .ofPattern("MMM dd, yyyy - HH:mm")
                        .withLocale(Locale.ENGLISH);

        LocalDateTime parsed = LocalDateTime.parse("Nov 03, 2014 - 07:13", formatter);
        String string = formatter.format(parsed);
        System.out.println(parsed);
        System.out.println(string);     // Nov 03, 2014 - 07:13


    }

}
