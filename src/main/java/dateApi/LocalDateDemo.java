package dateApi;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

/**
 * Created by doka on 2017-06-18.
 */
public class LocalDateDemo {
    public static void main(String[] args) {
        //--------------- Creating localDate ------------------------
        LocalDate today = LocalDate.now();
        System.out.println("today : " + today);
        LocalDate custom = LocalDate.of(2005,1,1);
        System.out.println("custom date : " + custom);
        //---------------- Parsing from string -----------------------
        DateTimeFormatter germanFormatter =
                DateTimeFormatter
                        .ofLocalizedDate(FormatStyle.MEDIUM)
                        .withLocale(Locale.GERMAN);

        LocalDate germanDate = LocalDate.parse("24.12.2014", germanFormatter);
        System.out.println("german formatter : " + germanDate );

        DateTimeFormatter stringFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate parsedDate = LocalDate.parse("2007/12/20",stringFormatter);
        System.out.println("custom formatter : " + parsedDate);

        //------------------- Getting details -------------------------
        DayOfWeek dayOfWeek = today.getDayOfWeek();
        System.out.println(dayOfWeek);
        System.out.println(today.getYear());


        //------------------- Date manipulation --------------------------
        LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
        System.out.println("tomorrow : " + tomorrow);
        LocalDate yesterday = tomorrow.minusDays(2);
        System.out.println("yesterday : " + yesterday);

        //-------------------Displaying in another format ---------------------
        DateTimeFormatter displayFormatter = DateTimeFormatter.ofPattern("yyyy:MM:dd");
        System.out.println("Today (another format) : " + today.format(displayFormatter));
    }
}
