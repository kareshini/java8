Files.java provides many useful functions.

* Files.list(Path dir) - lazily populated Stream, the elements of which are the entries in the directory. Not recursive.
* Files.lines(Path file) - put all lines into Stream. By default UTF-8, if you want another you have to pass it as argument.
* Files.readAllLines(Path file) - return List<String>
* Files.find() - return stream with files which match. Algorythm uses walk method in order to walk through file tree.
* Files.newBufferedReader(Path file).lines() - similar to Files.lines but this is lazily populated.
* Files.newBufferedWriter(...)
* create/copy/delete/move file,dir,tmpDir